#!/usr/bin/env python
"""
--------
This file is part of dns_scanner.py.

 dns_scanner.py is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 dns_scanner.py is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with dns_scanner.py.  If not, see <http://www.gnu.org/licenses/>.
--------

dns_scanner.py:
--------
This is a tool that has been written to write out the results of a DNS sweep
of the entire internet into a folder structure that is set as

$PWD/results/1/0/0/<result>

Where 1/0/0 is the folder for IP address in the range of 1.0.0.1 to 1.0.0.254

The range of addresses being scanned can be set by using the A = B = C = D =
constants at the top of this file.
--------
"""

import gevent
from gevent import socket
from gevent.pool import Pool
import time
import os
from datetime import datetime as date
from optparse import OptionParser


# adjust these lines to reflect the network ranges that you want to scan
A = [1, 224]
B = [0, 255]
C = [0, 255]
D = [1, 254]

FINISHED = 0
RESULTS = 'results/'
NO_PTR = 0
PTR = 0

def _send_event(result):
    """
    send a 0mq event to the reciever for the future distributed
    state using 0mq message bus for IPC.
    """
    return False

def commands():
    parser = OptionParser(add_help_option=False)
    parser.add_option('-h', '--help', action='help')
    parser.add_option('-s', '--sleep', default=True, action='store', type='int',
                      help='How long to sleep between subnets.', dest='sleep',
                      default=120)
    parser.add_option('-p', '--pool', default=True, action='store', type='int',
                      help='The size of the gevent pool (maximum 256).',
                      dest='pool', default=16)
    (options, args) = parser.parse_args()
    return options


def w_out(ip, result):
    """
    Write out the returned data into a folder structure
    records/1/0/0/results where the ip being scanned is
    1.0.0.1 to 1.0.0.255.
    """

    o = RESULTS

    for folder in ip.split('.')[:-1]:
        o = o + folder + '/'
        if not os.path.exists(o):
            os.makedirs(o)

    fn = o + str(ip.split('.')[3:][0])

    f = open(fn, 'w+')

    output = """ip address : %s \nptr result : %s \nin a result : %s\n""" % (
        str(ip), str(result[0]), str(result[1])
        )

    f.write(output)
    f.close()


def build_subnet(first, second, third):
    """
    returns a list of IP addresses
    """

    ip_list = []

    network = '%s.%s.%s' % (first, second, third)

    for ip in range(D[0], D[1]):
        l = network + '.' + str(ip)
        ip_list.append(l)

    return ip_list


def spawn_dns(ip):
    """
    function to spawn from gevent.spawn()
    """

    global FINISHED
    global NO_PTR
    global PTR

    try:
        try:
            ptr_result = socket.gethostbyaddr(ip)
            a_result = socket.gethostbyname(str(ptr_result[0]))
            result = [ptr_result[0], a_result]
            PTR += 1
            w_out(ip, result)

        except socket.gaierror as ex:
            NO_PTR += 1

        except socket.error as ex:
            NO_PTR += 1

    finally:
        FINISHED += 1


def do_dns(ip_list, poolsize):
    """
    The gevent coroutine to do the dns queries, size of pool addresses at a time.
    """

    pool = Pool(poolsize)

    with gevent.Timeout(180, False):
        for ip in ip_list:
            pool.spawn(spawn_dns, ip)

        pool.join()


def main():
    """
    main logic here
    """

    cmd = commands()
    sleep = cmd.sleep
    poolsize = cmd.pool

    start = time.time()
    print('Starting Scan at : %s') % (
        date.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        )

    os.environ['GEVENT_RESOLVER'] = 'ares'
    try:
        for first in range(A[0], A[1]):

            for second in range(B[0], B[1]):

                for third in range(C[0], C[1]):
                    print(' Starting to scan %s.%s.%s.0/24') %(
                    str(first), str(second), str(third)
                    )
                    ip_list = build_subnet(first, second, third)
                    do_dns(ip_list, poolsize)

                    time.sleep(sleep)

    except KeyboardInterrupt:
        print('\nExiting due to CTRL C\n')


    final = time.time()

    print('Finished Scan at : %s') % (
        date.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        )

    print('Total scan time : %s') % (final - start)
    print('Scanned %s IP addresses') % (FINISHED)
    print('IP Addresses with PTR : %s') % (PTR)
    print('IP Addresses with NO PTR : %s') % (NO_PTR)


if __name__ == "__main__":
    main()
