# dns_scanner.py

This tool has been written to scan the entire internet looking for PTR records. Once those PTR's are fetched it will do a reverse lookup check and store the data in a results folder.

This tool is not intended to be malicious but rather to track changes on the internet over time.

# Performance

This tool can scan a /16 network range in about 26 minutes on a Rackspace Cloud standard VM. To get the best performance possible please read below /etc/resolv.conf. In the code block there is a deliberate 5 second sleep between /24's. That is intended as an Internet good citizen sleep. (rather.. internet better citzen rather than good citizen).



# How-To

For this tool to work it has been tested with gevent 1.0 only on Python 2.7.x.

## Command Line

```bash
> git clone https://jlamb_priv@bitbucket.org/jlamb_priv/dns_scanner.git
> cd dns_scanner
> chmod +x dns_scanner.py
> ./dns_scanner --pool=128 --sleep=120
```

### --pool=

The --pool= flag represents how big the gevent poolsize is. setting it larger than 256 will do little extra as it only queue's one /24 at a time. The __default__ is __16__.

### --sleep=

The --sleep= flag tells how long to wait between each /24 lookup. The __default__ is __120 seconds__.

## /etc/resolv.conf

It is wise to ensure that your resolv.conf has the following items

```bash
 options rotate
 nameserver <name.server.ip.address.1>
 nameserver <name.server.ip.address.2>
 nameserver <name.server.ip.address.3>
```

The options rotate syntax at the top will round-robin the name servers so that you do not overload your primary name server with DNS queries. The default resolv.h limits the numbers of name servers to 3.

## Using SCREEN

This tool has been written to use within screen. That way you can check the process on the CLI without needing your window to be open/internet connection to be reliable.

## Public Name Servers

Please do not use public name servers like Google or OpenDNS. Remember to be a good Internet citizen.